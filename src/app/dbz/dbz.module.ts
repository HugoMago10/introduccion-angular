import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';

import { FormsModule } from '@angular/forms';
import { PersonajesComponent } from './personajes/personajes.component';
import { AgregarComponent } from './agregar/agregar.component';
import { DbzService } from './services/dbz.service';



@NgModule({
  declarations: [
    MainPageComponent,
    PersonajesComponent,
    AgregarComponent
  ],
  exports:[MainPageComponent],//se tiene que exportar para que se pueda usar en otro modulo
  imports: [
    CommonModule,
    FormsModule
  ],
  providers:[//funcionan como singleton
    DbzService//servicio 
  ]
})
export class DbzModule { }
