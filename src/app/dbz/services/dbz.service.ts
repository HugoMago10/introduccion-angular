import { Injectable } from "@angular/core";
import { Personaje } from "../interfaces/dbz.interface";

@Injectable()//esta clase es apta para inyectarse
export class DbzService {

    //utilizando el interfaz Personaje
    private _personajes: Personaje[] = [{
        nombre: "goku",
        poder: 2700
    }, {
        nombre: "vegueta",
        poder: 2500
    }];

    constructor() {
        console.log("Servicio inicializado");
    }

    get personajes(){
        //manda una copia del arreglo para que no modifiquen el arreglo original
        return [...this._personajes];
    }

    agregarPersonaje (personaje : Personaje){
        this._personajes.push (personaje);
    }
}