import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
})
export class AgregarComponent {

  @Input() nuevo : Personaje = {nombre:"Piccolo",poder:2700};
  //creando un evento llamado onNuevoPersonaje
  @Output() onNuevoPersonaje:EventEmitter<Personaje> = new EventEmitter();

  constructor (private dbzService : DbzService){

  }

  agregar( ){
    if (this.nuevo.nombre.trim().length===0){return;}

    /* this.onNuevoPersonaje.emit( this.nuevo )//se le pasa Personaje
    console.log( this.nuevo ) */

    this.dbzService.agregarPersonaje( this.nuevo );


    this.nuevo={nombre:"",poder:0};//reinicia objeto
  }
}