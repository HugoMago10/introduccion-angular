import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ContadorComponent } from "./contador.component";

@NgModule({
    declarations:[ContadorComponent],//se usa el componente contador
    exports:[ContadorComponent],
    imports:[//exporta para poder usarlo fuera de este modulo
        CommonModule //->sirve para los ngFor ngIf, etc.
    ],
    providers:[]
})

//se usara este modulo en app.module.ts para reflejar los cambios
export class ContadorModule { }