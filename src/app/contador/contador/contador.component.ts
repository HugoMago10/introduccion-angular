import {Component} from "@angular/core";

//se crea un componente
@Component({
    selector:"app-contador", //nombre de la etiqueta
    template:`
        <h1>{{ title }}</h1>
        <button (click)="numero = numero +1">+1</button>
        <p>{{numero}}</p>
        <button (click)="numero = numero -1">-1</button>
        
        <button (click)="sumar(12)">suma +12</button>
        <button (click)="sumar(-12)">suma -12</button>
        
        <br><br>
        <button (click)="base = base+5">suma +{{base}}</button>
        <p>{{base}}</p>
        <button (click)="base = base-5">suma -{{base}}</button>
    `
})
//componente que se usara en contador.module.ts
export class ContadorComponent{
    //propiedades
    title = 'contador';
    numero: number = 0;

    base: number = 5;

    sumar(valor:number){
        this.numero = this.numero + valor;
    }
}