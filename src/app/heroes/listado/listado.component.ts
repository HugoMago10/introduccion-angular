import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html'
})
export class ListadoComponent {
  heroes:string[] =["Spider man","Iron Man","Hulk","Thor","Panther","Capitan America"]

  nombre:string="";

  borraElemento(){
    const heroe = this.heroes.pop();
    this.nombre = heroe === undefined ? "": heroe;
  }

}
