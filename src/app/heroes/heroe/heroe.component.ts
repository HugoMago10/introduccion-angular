import {Component} from "@angular/core";

@Component({
    selector:"app-heroe",
    templateUrl: 'heroe.component.html'
})

export class HeroeComponent{
    nombre : string  = "Ironman";
    edad : Number  = 45;

    get nombreCapitalize(){
        return this.nombre.toUpperCase();
    }

    obtenerNombre():string{
        return `
            ${this.nombre} - ${this.edad}
        `;
    }

    cambiarNombre(){
        this.nombre = "spiderman";
    }
    cambiarEdad(){
        this.edad = 32;
    }
}