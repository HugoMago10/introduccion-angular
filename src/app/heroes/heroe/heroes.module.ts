import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeroeComponent } from './heroe.component';
import { ListadoComponent } from '../listado/listado.component';

@NgModule({
    declarations: [
        HeroeComponent,
        ListadoComponent
    ],
    exports: [//cosas que quiero que sean publicas
        ListadoComponent
    ],
    imports: [//solo modulos
        CommonModule //->sirve para los ngFor ngIf, etc.
    ],
    providers: [],
  })

//export porque se usara en otro archivo
export class HeroesModule { }