import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ContadorModule } from './contador/contador/contador.module';
import { DbzModule } from './dbz/dbz.module';
import { HeroesModule } from './heroes/heroe/heroes.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [//van solo modulos
    BrowserModule, 
    HeroesModule,
    ContadorModule,//importando el contadorModule
    DbzModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
